from flask import Flask, render_template, send_file

import pandas as pd
import docx
from datetime import date
import requests
from bs4 import BeautifulSoup

def create_list():
    
    urls = {
        '155310':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xDD37CFE4D67E964F995148ECEA1CF20E&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154310':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0x68DBC414FC4BB74CA2F209765BF10933&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154110':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xED44C8C10685D51196700000F4B4937D&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154220':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xF144C8C10685D51196700000F4B4937D&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '155910':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xC7AFA419DCAEF14881D2DE5DDDFBFE07&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154610':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xF944C8C10685D51196700000F4B4937D&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '157010':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0x7F39EFA0210ABF48A4EFC0615759064F&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '156620':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xD6C83D4CED0C3E469552588F802B87B3&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154810':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xFD44C8C10685D51196700000F4B4937D&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9',
        '154005':'http://www.campus.rwth-aachen.de/rwth/all/unit.asp?gguid=0xA1BAF3F28F2AD611BDB90002A5871170&mode=unit&tguid=0xEBB2D1C29613C04FBF47F82813B5A4E9'
        }
    
    Telefonliste = pd.DataFrame(columns=['Name', 'Telefon', 'E-Mail', 'IKZ'])

    for key, value in urls.items():
        r = requests.get(value)
        soup = BeautifulSoup(r.content, features='html.parser')
        table = soup.find_all('table')
        df = pd.read_html(str(table))[8]
        df = df[df['E-Mail'].str.contains('@')==True]
        df = df[df['Telefon'].str.contains('@')==False]
        df['IKZ'] = key
        Telefonliste = pd.concat([Telefonliste, df])
        
    Telefonliste.sort_values('Name', inplace=True)
    Telefonliste.drop_duplicates(subset='Name', keep="first", inplace=True)
    
    return Telefonliste
    
def save_list(Telefonliste):
        
    doc = docx.Document()

    doc.add_heading('ITMC Telefonliste Stand ' +str(date.today()), level=0)

    t = doc.add_table(Telefonliste.shape[0]+1, Telefonliste.shape[1], style='Light Grid')

    for j in range(Telefonliste.shape[-1]):
        t.cell(0,j).text = Telefonliste.columns[j]
    
    for i in range(Telefonliste.shape[0]):
        for j in range(Telefonliste.shape[-1]):
            t.cell(i+1,j).text = str(Telefonliste.values[i,j])
        
    doc.save('./app/Telefonliste.docx')
    
    return

app = Flask(__name__)

@app.route('/', methods=("POST", "GET"))
def index():
    
    Telefonliste = create_list()
        
    save_list(Telefonliste)
        
    return render_template('index.html', tables=[Telefonliste.to_html(classes='table table-striped', header=True, index=False)])
    
@app.route('/download')
def downloadFile ():

    path = 'Telefonliste.docx'

    return send_file(path, as_attachment=True)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8084)